# alta3research-ansible-cert
The objective is to obtain certification from Alta3 Research to show proficiency with Ansible.


## Getting started

Deply using: ansible-playbook alta3research-ansiblecert01.yml


## Prerequisites


## Built With

* [Ansible](https://www.ansible.com) - ansible [core 2.12.4]
* [Python](https://www.python.org/) - 3.8.10 (default, Mar 15 2022, 12:22:08) [GCC 9.4.0]


## Authors

* **Malathy Ramani**



